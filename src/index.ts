import loggerFactory from './lib/logger-factory';

const logger = loggerFactory.getLogger(__filename);

(async () => {
  try {
    logger.info({ message: 'Application initialized' });
  } catch (err) {
    logger.error({ message: err.stack || err });
  }

})();
